#ifndef OPTIONS_H
#define OPTIONS_H

typedef struct Config {
    long int max_iter;
    double tolerance;
    int runtime_stats;
    char* path_coeff_mat;
    char* path_const_vec;
    char* path_result_vec;
} Config;

int parse_options(int argc, char **argv, struct Config *cfg, int prank);

#endif /* OPTIONS_H */


