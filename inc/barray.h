#include <stdint.h>

#ifndef BARRAY_H
#define BARRAY_H

typedef double elem_t;

int read_array_double(char *path, uint32_t *dnum, uint32_t **dsizes,
        elem_t **array, uint32_t *cmp_dnum, uint32_t *cmp_dsizes);
int write_array_double(char *path, elem_t *array, uint32_t dnum,
        uint32_t *dsizes);

#endif /* BARRAY_H */


