#include <stdbool.h>

#include "barray.h"

#ifndef HELPER_H
#define HELPER_H

bool check_min_distance(elem_t *a, elem_t *b, int n, double tol);
int load_lin_eq_sys(char *path_A, char *path_b, elem_t **A, elem_t **b, int *n);

#endif /* HELPER_H */


