# Source code for the master thesis

This repo contains different implementations of a (vectorized) distributed jacobi method.
To show the differences between the implementations more easily they are all organized in their own branch.

## Overview of branches

### master

Distributed jacobi method.

### avx

Base on branch master. Distributed and vectorized jacobi method.

### avx_aligned

Based on branch avx. The data buffers are 256bit-aligned to allow aligned memory access of the avx intrinsics.

### avx_ps

Based on branch avx. The data type of the elements is changed from double (packed double-precision) to float (packed single-precision).

### avx_colwise

Based on branch avx. The calculation loop processes four lines of the local coefficient matrix at once using the scattered load intrinsics.

### avx_pack

Based on branch avx_colwise. The local buffer of the coefficient matrix is restructured with MPI_Unpack to avoid scattered loads in favor of continuous loads.

### avx_mpi_type

Based on branch avx_pack. Instead of copying the entire buffer with MPI_Unpack the restructuring of the local buffer of the coefficient matrix is done while distributing the date with MPI_Scatterv.

### avx_mpi_io

Based on branch avx. The loading and distribution of the input data is implemented with MPI-IO. 

## Requirements

- CPU/Nodes which support AVX2 ([List of supported architectures](https://en.wikipedia.org/wiki/Advanced_Vector_Extensions#CPUs_with_AVX2))
- openmpi (tested with v4.1.4)
- gcc (tested with v10.2.0)
- make (tested with v4.3-3)
- rust (tested with 1.63), required for the utility tools to handle the binary input/output files

## Getting started

Clone the project
```
git clone https://gitlab.com/_bryne/distributed-jacobi-method.git
```
Switch to the preferred branch ([Overview of branches](#overview-of-branches))
```
git checkout <branch>
```
Build the current branch. The binary is then located in the build directory.
```
make
```
Solve a randomly generated system of linear equations of size `<size>`. If the SIZE argument is omitted the previously generated data will be used if available. Generated data of branch avx_ps cannot be used in other branche because of the different data type.
```
make test SIZE=<size>
```
