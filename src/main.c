#include <errno.h>
#include <mpi.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "barray.h"
#include "helper.h"
#include "options.h"

typedef enum Exit_t {
    Success,
    Failure,
    Abort
} Exit;

int main (int argc, char **argv)
{
    int psize;
    int prank;
    elem_t *A = NULL;
    elem_t *b = NULL;
    elem_t *x = NULL;
    elem_t *xold = NULL;
    elem_t *xnew = NULL;
    int n;
    int block_size;
    int *block_sizes = NULL;
    int *block_displs = NULL;
    Exit exit_t = Success;

    double prev_time = 0.0;
    double times[4] = {0.0, 0.0, 0.0, 0.0};

    /* Default configuration */
    Config cfg = {
        .max_iter = 1000,
        .tolerance = 1e-6,
        .runtime_stats = 0,
        .path_coeff_mat = NULL,
        .path_const_vec = NULL,
        .path_result_vec = "result.bin"
    };

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &psize);
    MPI_Comm_rank(MPI_COMM_WORLD, &prank);

    /* Get configuration from command line arguments */
    if (parse_options(argc, argv, &cfg, prank) != 0) {
        exit_t = Failure;
        goto free_and_exit;
    }

    if (prank == 0 && cfg.runtime_stats == 1) {
        times[0] = MPI_Wtime();
    }

    /* Load matrix A and vector b */
    if (prank == 0) {
        if (load_lin_eq_sys(cfg.path_coeff_mat, cfg.path_const_vec, &A, &b, &n)
                != 0) {
            fprintf(stderr, "Loading of input data failed\n");
            exit_t = Abort;
            goto free_and_exit;
        }

        if (cfg.runtime_stats == 1) {
            times[1] = MPI_Wtime() - times[0];
            prev_time = MPI_Wtime();
        }
    }

    /* Begin jacobi method */

    /* Broadcast size of the system of linear equations */
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

    /* Calculate number of rows per process */
    block_size = (n + prank) / psize;

    block_sizes = malloc(sizeof(int) * psize);

    if (block_sizes == NULL) {
        fprintf(stderr, "%s\n", strerror(errno));
        exit_t = Abort;
        goto free_and_exit;
    }

    block_displs = malloc(sizeof(int) * psize);

    if (block_displs == NULL) {
        fprintf(stderr, "%s\n", strerror(errno));
        exit_t = Abort;
        goto free_and_exit;
    }

    MPI_Allgather(&block_size, 1, MPI_INTEGER, block_sizes, 1,
            MPI_INTEGER, MPI_COMM_WORLD);

    int block_displ = 0;

    for (int p = 0; p < psize; ++p) {
        block_displs[p] = block_displ;
        block_displ += block_sizes[p];
    }

    /* Allocate local buffers for matrix A and vector b */
    if (prank != 0) {
        A = malloc(sizeof(elem_t) * block_size * n);

        if (A == NULL) {
            fprintf(stderr, "%s\n", strerror(errno));
            exit_t = Abort;
            goto free_and_exit;
        }

        b = malloc(sizeof(elem_t) * block_size);

        if (b == NULL) {
            fprintf(stderr, "%s\n", strerror(errno));
            exit_t = Abort;
            goto free_and_exit;
        }
    }

    x = calloc(n, sizeof(elem_t));

    if (x == NULL) {
        fprintf(stderr, "%s\n", strerror(errno));
        exit_t = Abort;
        goto free_and_exit;
    }

    xold = malloc(n * sizeof(elem_t));

    if (xold == NULL) {
        fprintf(stderr, "%s\n", strerror(errno));
        exit_t = Abort;
        goto free_and_exit;
    }

    xnew = malloc(block_size * sizeof(elem_t));

    if (xnew == NULL) {
        fprintf(stderr, "%s\n", strerror(errno));
        exit_t = Abort;
        goto free_and_exit;
    }

    MPI_Datatype row;
    MPI_Type_vector(n, 1, 1, MPI_DOUBLE, &row);
    MPI_Type_commit(&row);

    /* Scatter matrix A and vector b to all processes */
    MPI_Scatterv(A, block_sizes, block_displs, row, A, block_size,
            row, 0, MPI_COMM_WORLD);
    MPI_Scatterv(b, block_sizes, block_displs, MPI_DOUBLE, b, block_size,
            MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (prank == 0 && cfg.runtime_stats == 1) {
        times[2] = MPI_Wtime() - prev_time;
        prev_time = MPI_Wtime();
    }

    int iter = 0;

    do {
        for (int row = 0; row < block_size; ++row) {
            int diag_elem_idx = block_displs[prank] + row;
            int row_start_idx = n * row;

            xnew[row] = b[row];

            /* Calculate product sum up to the diagonal element */
            for (int elem_idx = 0; elem_idx < diag_elem_idx; ++elem_idx) {
                xnew[row] -= A[row_start_idx + elem_idx] * x[elem_idx];
            }

            /* Calculate product sum following the diagonal element */
            for (int elem_idx = diag_elem_idx + 1; elem_idx < n; ++elem_idx) {
                xnew[row] -= A[row_start_idx + elem_idx] * x[elem_idx];
            }

            /* Divide product sum by the diagonal element */
            xnew[row] /= A[row_start_idx + diag_elem_idx];
        }

        /* Save old x vector for tolerance comparison */
        memcpy(xold, x, n * sizeof(elem_t));

        /* Gather all local values of vector x in each process */
        MPI_Allgatherv(xnew, block_size, MPI_DOUBLE, x, block_sizes,
                block_displs, MPI_DOUBLE, MPI_COMM_WORLD);
        ++iter;
    } while (iter < cfg.max_iter && !check_min_distance(x, xold, n, cfg.tolerance));

    if (prank == 0) {
        times[3] = MPI_Wtime() - prev_time;

        if (!check_min_distance(x, xold, n, cfg.tolerance)) {
            printf("Tolerance of %e could not be reached in %d iteration(s)!\n",
                    cfg.tolerance, iter);
        }

        if (cfg.runtime_stats == 1) {
            printf("Runtime:\t\tLOAD\tDIST\tCALC\tTOTAL\n");
            printf("\t\t\t%.4lf\t%.4lf\t%.4lf\t%.4lf\n", times[1], times[2],
                    times[3], times[3] + prev_time - times[0]);
            printf("Iterations:\t\t%d (max. %ld)\n", iter, cfg.max_iter);
            printf("Processes:\t\t%d\n", psize);
            printf("Linear Equations:\t%d\n", n);

            printf("\nRank Blocksize Blockdispl\n");

            for (int i = 0; i < psize; ++i) {
                printf("%4d %9d %10d\n", i, block_sizes[i], block_displs[i]);
            }
        }

        write_array_double(cfg.path_result_vec, x, 1, (uint32_t*) &n);
    }

free_and_exit:
    free(A);
    free(b);
    free(x);
    free(xnew);
    free(xold);
    free(block_sizes);
    free(block_displs);

    if (exit_t == Abort) {
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }

    MPI_Finalize();

    if (exit_t == Failure) {
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}


