/**
 * @file barray.h
 * @brief Simple library to read and write n-dimensional arrays of double
 *        precision floating point numbers (IEEE 754) from and to binary files.
 *
 * A binary file has an header section starting with a 4 byte unsigned integer
 * (uint32_t) describing the number of array dimensions. This number is followed
 * by one uint32_t for every dimension defining its dimension size. The header
 * section ends with zeroed pad bytes to align the subsequent data section to
 * the size of one double precision floating point (double) data element. The
 * data section is the concatenation of all double elements of the array.
 *
 * ---------------------------------------------------------------------
 * | dnum | dsize_0 | ... | dsize_n | (padding) | elem_0 | elem_1 | ...
 * ---------------------------------------------------------------------
 *
 * @warning
 *
 * All binary data is written in native byte order. Keep that in mind
 * when machines with different byte orders access the binary data!
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "barray.h"

/**
 * @brief Get the distance in bytes to the next address which fullfills the
 *        specified alignment requirement in bytes.
 *
 * @param[in] current   Current address position.
 * @param[in] align     Alignment requirment.
 *
 * @return number of bytes to the next aligned position.
 */
long bytes_to_align(long current, long align)
{
    return (align - (current % align)) % align;
}

/**
 * @brief Read n dimensional array from binary file.
 *
 * @param[in]  path         Path to the binary file.
 * @param[out] dnum         Number of dimensions.
 * @param[out] dsizes       Sizes of each dimension.
 * @param[out] array        Array elements.
 * @param[in]  cmp_dnum     Expected number of dimensions or NULL.
 * @param[in]  cmp_dsizes   Expected sizes of each dimension or NULL.
 *
 * @return 0 on sucess, -1 on error.
 */
int read_array_double(char *path, uint32_t *dnum, uint32_t **dsizes,
        elem_t **array, uint32_t *cmp_dnum, uint32_t *cmp_dsizes)
{
    FILE *fp = fopen(path, "rb");

    if (fp == NULL) {
        fprintf(stderr, "%s.\n", strerror(errno));
        return -1;
    }

    unsigned char buf[8];

    /* Read number of dimensions from file */
    if (fread(buf, 4, 1, fp) != 1) {
        fprintf(stderr, "Unable to read number of dimensions\n");
        fclose(fp);
        return -1;
    }

    uint32_t real_dnum = *((uint32_t*) buf);

    if (real_dnum == 0) {
        fprintf(stderr, "Invalid number of dimensions of 0\n");
        fclose(fp);
        return -1;
    }

    if (cmp_dnum != NULL && *cmp_dnum != real_dnum) {
        fprintf(stderr, "Array has unexpected number of dimensions\n");
        fclose(fp);
        return -1;
    }

    size_t elem_num = 1;

    uint32_t *real_dsizes = malloc(real_dnum * sizeof(uint32_t));

    if (real_dsizes == NULL) {
        fprintf(stderr, "%s\n", strerror(errno));
        fclose(fp);
        return -1;
     }

    /* Read sizes of every dimension from file and check for validity */
    for (uint32_t i = 0; i < real_dnum; ++i) {
        if (fread(buf, 4, 1, fp) != 1) {
            fprintf(stderr, "Unable to read dimension size!\n");
            fclose(fp);
            free(real_dsizes);
            return -1;
        }

        real_dsizes[i] = *((uint32_t*) buf);

        if (real_dsizes[i] == 0) {
            fprintf(stderr, "Invalid dimension size of 0!\n");
            fclose(fp);
            free(real_dsizes);
            return -1;
        }

        if (cmp_dsizes != NULL && cmp_dsizes[i] != real_dsizes[i]) {
            fprintf(stderr, "Array has wrong dimension size!\n");
            fclose(fp);
            free(real_dsizes);
            return -1;
        }

        elem_num *= real_dsizes[i];
    }

    /* Skip padding */
    long current_offset = ftell(fp);

    if (current_offset == 0) {
        fprintf(stderr, "%s\n", strerror(errno));
        return -1;
    }

    long diff = bytes_to_align(current_offset, 8);

    if(fseek(fp, diff, SEEK_CUR) != 0) {
        fprintf(stderr, "%s\n", strerror(errno));
        fclose(fp);
        return -1;
    }

    elem_t *arr = malloc(elem_num * sizeof(elem_t));

    if (arr == NULL) {
        fprintf(stderr, "%s\n", strerror(errno));
        fclose(fp);
        free(real_dsizes);
        return -1;
     }

    /* Read all array elements from the file */
    for (size_t i = 0; i < elem_num; ++i) {
        if (fread(buf, 8, 1, fp) != 1) {
            fprintf(stderr, "Unable to read array element!\n");
            fclose(fp);
            free(real_dsizes);
            free(arr);
            return -1;
        }

        arr[i] = *((elem_t*) buf);
    }

    *dnum = real_dnum;
    *dsizes = real_dsizes;
    *array = arr;
    return 0;
}

/**
 * @brief Write n dimensional array to binary file
 *
 * @param[in] path      Path of binary file.
 * @param[in] array     Array data.
 * @param[in] dnum      Number of dimensions.
 * @param[in] dsizes    Array of all dimension sizes.
 *
 * @return 0 on sucess, -1 on error.
 */
int write_array_double(char *path, elem_t *array, uint32_t dnum,
        uint32_t *dsizes)
{
    FILE *fp = fopen(path, "wb");

    if (fp == NULL) {
        fprintf(stderr, "%s.\n", strerror(errno));
        return -1;
    }

    /* Write number of dimensions */
    if (fwrite(&dnum, 4, 1, fp) != 1) {
        fprintf(stderr, "Unable to write the header data\n");
        fclose(fp);
        return -1;
    }

    /* Write dimension sizes */
    if (fwrite(dsizes, 4, dnum, fp) != dnum) {
        fprintf(stderr, "Unable to write the header data\n");
        fclose(fp);
        return -1;
    }

    if (dsizes == NULL) {
        fprintf(stderr, "Array of dimension sizes is empty");
        return -1;
    }

    /* Write pad bytes to align the data section */
    long current_offset = ftell(fp);

    if (current_offset == 0) {
        fprintf(stderr, "%s\n", strerror(errno));
        return -1;
    }

    long diff = bytes_to_align(current_offset, 8);
    uint8_t pad_byte = 0x00;

    if (fwrite(&pad_byte, 1, diff, fp) != (size_t) diff) {
        fprintf(stderr, "Unable to write the pad bytes\n");
        fclose(fp);
        return -1;
    }

    size_t n = 1;

    for (uint32_t i = 0; i < dnum; ++i) {
        n *= dsizes[i];
    }

    /* Write the vector data */
    if (fwrite(array, 8, n, fp) != n) {
        fprintf(stderr, "Unable to write the array data\n");
        fclose(fp);
        return -1;
    }

    return 0;
}


