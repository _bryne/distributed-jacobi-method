#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "barray.h"
#include "helper.h"

/*
 * @brief Check if the difference between each element pair with the same index
 *        of both arrays is less than the specified tolerance.
 *
 * @param[in] a     Vector a.
 * @param[in] b     Vector b.
 * @param[in] n     Size of both vectors.
 * @param[in] tol   Tolerance.
 *
 * @return true if each difference is less then the specified tolerance.
 *         Otherwise return false.
 */
bool check_min_distance(elem_t* a, elem_t* b, int n, double tol)
{
    for (int i = 0; i < n; ++i) {
        double diff = fabs(a[i] - b[i]);

        if (diff > tol) {
            return false;
        }
    }
    return true;
}

/**
 * @brief Load a square coefficient matrix and a constant vector with same row
 *        size from binary array files.
 *
 * @param[in]   path_A  Path to binary file containing the coefficient matrix.
 * @param[in]   path_b  Path to binary file containing the constant vector.
 * @param[out]  A       Array of all elements of matrix A.
 * @param[out]  b       Array of all elements of vector b.
 * @param[out]  n       Dimension size of matrix A and vector b.
 *
 * @return 0 on success, -1 on error.
 */
int load_lin_eq_sys(char *path_A, char *path_b, elem_t **A, elem_t **b, int *n)
{
    uint32_t cmp_A_dnum = 2;

    uint32_t A_dnum;
    uint32_t *A_dsizes = NULL;;

    /* Load coefficient matrix */
    if (read_array_double(path_A, &A_dnum, &A_dsizes, A, &cmp_A_dnum, NULL)
            != 0) {
        fprintf(stderr, "Cannot load coefficient matrix from file '%s'\n",
                path_A);
        free(A_dsizes);
        return -1;
    }

    /* Check if matrix is square */
    if (A_dsizes[0] != A_dsizes[1]) {
        fprintf(stderr, "Coefficent matrix not square\n");
        free(A_dsizes);
        return -1;
    }

    uint32_t cmp_b_dnum = 1;

    uint32_t b_dnum;
    uint32_t *b_dsizes = NULL;

    /* Load constant vector with same size as the matrix dimensions */
    if (read_array_double(path_b, &b_dnum, &b_dsizes, b, &cmp_b_dnum,
            A_dsizes) != 0) {
        fprintf(stderr, "Cannot load constant vector from file '%s'\n", path_b);
        free(A_dsizes);
        free(b_dsizes);
        return -1;
    }

    *n = A_dsizes[0];

    free(A_dsizes);
    free(b_dsizes);

    return 0;
}


