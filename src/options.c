#include <errno.h>
#include <getopt.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "options.h"

void print_help_msg(void)
{
    printf("\
Usage: jac [OPTIONS]... FILE1 FILE2\n\
Solve systems of linear equations with the distributed jacobi method.\n\
Load the coefficient matrix from FILE1 and the constant vector from FILE2.\n\
\n");
    printf("\
-i, --max-iterations=MAX    do a maximum of MAX iterations\n\
-o  --output-file=FILE      write the results to FILE\n\
-s, --runtime-statistics    print various runtime statistics like execution\n\
                              time and data distribution\n\
-t, --tolerance=TOL         try to reach a maximal delta of TOL between two\n\
                              iterations for every result vector element\n\
\n\
    --help      display this help\n\n");
    printf("\
FILE1 and FILE2 are expected to store arrays in the following binary format.\n\
The file begins with a header section starting with a 4 byte unsigned integer\n\
(uint32_t) describing the number of array dimensions. This number is followed\n\
by one uint32_t for every dimension defining its dimension size. The header\n\
section ends with zeroed pad bytes to align the subsequent data section to\n\
the size of one double precision floating point (ieee 754, double) data element.\n\
The data section is the concatenation of all double elements of the array.\n");

    return;
}

int parse_options(int argc, char **argv, struct Config *cfg, int prank)
{
    int opt_result;
    char *endptr;
    int errno_old;

    struct option long_options[] = {
        {"help",                no_argument,       0, 'h' },
        {"max-iterations",      required_argument, 0, 'i' },
        {"output-file",         required_argument, 0, 'o' },
        {"runtime-statistics",  no_argument,       0, 's' },
        {"tolerance",           required_argument, 0, 't' },
        {0,                     0,                 0,  0  }
    };

    while (1) {
        opt_result = getopt_long(argc, argv, "i:st:o:", long_options, NULL);

        if (opt_result == -1) {
            break;
        }

        switch (opt_result) {

            case 'h':
                if (prank == 0) {
                    print_help_msg();
                }

                return -1;

            case 'i':
                if (optarg == NULL) {
                    fprintf(stderr, "optarg is NULL\n");
                    return -1;
                }

                errno = 0;
                long int max_iter = strtol(optarg, &endptr, 0);
                errno_old = errno;

                if (errno_old != 0) {
                    fprintf(stderr, "%s: Invalid number of iterations: '%s': ",
                            argv[0], optarg);
                    fprintf(stderr, "%s\n", strerror(errno_old));
                    return -1;
                }

                if (endptr == optarg || *endptr != '\0') {
                    fprintf(stderr, "%s: Invalid number of iterations: '%s': ",
                            argv[0], optarg);
                    fprintf(stderr, "Cannot convert to integer\n");
                    return -1;
                }

                if (max_iter <= 0) {
                    fprintf(stderr, "%s: Invalid number of iterations: '%s': ",
                            argv[0], optarg);
                    fprintf(stderr, "Less equal zero\n");
                    return -1;
                }

                cfg->max_iter = max_iter;
                break;

            case 's':
                cfg->runtime_stats = 1;
                break;

            case 't':
                if (optarg == NULL) {
                    fprintf(stderr, "optarg is NULL\n");
                    return -1;
                }

                errno = 0;
                double tolerance = strtod(optarg, &endptr);
                errno_old = errno;

                if (errno_old != 0) {
                    fprintf(stderr, "%s: Invalid tolerance: '%s': ",
                            argv[0], optarg);
                    fprintf(stderr, "%s\n", strerror(errno_old));
                    return -1;
                }

                if (endptr == optarg || *endptr != '\0') {
                    fprintf(stderr, "%s: Invalid tolerance: '%s': ",
                            argv[0], optarg);
                    fprintf(stderr, "Cannot convert to floating number\n");
                    return -1;
                }

                if (!isnormal(tolerance)) {
                    fprintf(stderr, "%s: Invalid tolerance: '%s': ",
                            argv[0], optarg);
                    fprintf(stderr, "Less equal zero\n");
                    return -1;
                }

                cfg->tolerance = tolerance;
                break;

            case 'o':
                if (optarg == NULL) {
                    fprintf(stderr, "optarg is NULL\n");
                    return -1;
                }

                cfg->path_result_vec = optarg;
                break;

            case '?':
                fprintf(stderr, "Use '%s --help' for help\n", argv[0]);
                return -1;

            default:
                fprintf(stderr, "Unexpected option character\n");
                return -1;
        }
    }

    if ((optind + 2) != argc) {
        fprintf(stderr, "Invalid number of arguments\n");
        fprintf(stderr, "Use '%s --help' for help\n", argv[0]);
        return -1;
    }

    cfg->path_coeff_mat = argv[optind];
    cfg->path_const_vec = argv[optind + 1];

    return 0;
}


