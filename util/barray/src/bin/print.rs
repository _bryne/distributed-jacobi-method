use barray::read_array;
use std::env;
use std::error::Error;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("Invalid number of arguments");
        eprintln!("Usage: print FILE");
        std::process::exit(1);
    }

    match read_array(&args[1]) {
        Ok(array) => println!("{:#.6}", array),
        Err(error) => match error.source() {
            Some(src) => eprintln!("{}: {}", error, src),
            None => {
                eprintln!("{}", error);
                std::process::exit(1);
            }
        },
    }
    std::process::exit(0);
}
