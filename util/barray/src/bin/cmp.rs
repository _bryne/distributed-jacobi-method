use barray::read_array;
use std::env;
use std::error::Error;
use std::str::FromStr;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 4 {
        eprintln!("Invalid number of arguments");
        eprintln!("Usage: cmp FILE1 FILE2 TOLERANCE");
        std::process::exit(1)
    }

    let array1 = match read_array(&args[1]) {
        Ok(array) => array,
        Err(error) => {
            match error.source() {
                Some(src) => eprintln!("{}: {}", error, src),
                None => eprintln!("{}", error),
            }
            std::process::exit(1)
        }
    };

    let array2 = match read_array(&args[2]) {
        Ok(array) => array,
        Err(error) => {
            match error.source() {
                Some(src) => eprintln!("{}: {}", error, src),
                None => eprintln!("{}", error),
            }
            std::process::exit(1)
        }
    };

    if array1.shape() != array2.shape() {
        eprintln!("Unable to compare: Shapes of arrays are not equal");
        std::process::exit(1)
    }

    let tol = match f64::from_str(&args[3]) {
        Ok(t) => t,
        Err(e) => {
            eprintln!("Unable to parse tolerance: {}", e);
            std::process::exit(1)
        }
    };

    if array1.abs_diff_eq(&array2, tol) == true {
        println!("Equal within tolerance");
    } else {
        println!("Not equal within tolerance");
    }

    std::process::exit(0)
}
