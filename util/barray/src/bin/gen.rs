use barray::write_array;
use ndarray::{Array, Array1, Array2};
use ndarray_linalg::solve::Solve;
use ndarray_rand::rand_distr::StandardNormal;
use ndarray_rand::RandomExt;
use std::env;
use std::error::Error;
use std::str::FromStr;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 3 {
        eprintln!("Invalid number of arguments");
        eprintln!("Usage: gen SIZE PATH");
        std::process::exit(1)
    }

    let size = match usize::from_str(&args[1]) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(1)
        }
    };

    let mut a: Array2<f64> = Array::random((size, size), StandardNormal);

    // Satisfy the property of a strict diagonally dominant matrix by adding up
    // the absolute values of all elements of the same row (except for the
    // diagonal element) on the diagonal element, which is initialized to 1.
    for (j, mut row) in a.outer_iter_mut().enumerate() {
        row[j] = 1f64;
        for k in 0..row.len() {
            if j != k {
                row[j] += row[k].abs();
            }
        }
    }

    let b: Array1<f64> = Array::random(size, StandardNormal);

    let x = a.solve(&b).unwrap();

    let mut base_path = String::from(&args[2]);

    if base_path.ends_with('/') != true {
        base_path.push('/');
    }

    // Write all three arrays to files
    match write_array(&(base_path.clone() + "A.bin"), &a) {
        Ok(()) => (),
        Err(error) => match error.source() {
            Some(src) => eprintln!("{}: {:?}", error, src),
            None => {
                eprintln!("{}", error);
                std::process::exit(1)
            }
        },
    }

    match write_array(&(base_path.clone() + "b.bin"), &b) {
        Ok(()) => (),
        Err(error) => match error.source() {
            Some(src) => eprintln!("{}: {:?}", error, src),
            None => {
                eprintln!("{}", error);
                std::process::exit(1)
            }
        },
    }

    match write_array(&(base_path.clone() + "x.bin"), &x) {
        Ok(()) => (),
        Err(error) => match error.source() {
            Some(src) => eprintln!("{}: {:?}", error, src),
            None => {
                eprintln!("{}", error);
                std::process::exit(1)
            }
        },
    }

    std::process::exit(0);
}
