use std::error;
use std::fmt;

#[derive(Debug)]
pub enum Error {
    FileNotOpenable(std::io::Error),
    HeaderNotParsable(std::io::Error),
    DataNotParsable(std::io::Error),
    FailedWrite(std::io::Error),
    UnexpectedData,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::FileNotOpenable(_) => write!(f, "Cannot open file"),
            Error::HeaderNotParsable(_) => write!(f, "Failed to parse array header"),
            Error::DataNotParsable(_) => write!(f, "Failed to parse array data"),
            Error::FailedWrite(_) => write!(f, "Unable to write array"),
            Error::UnexpectedData => write!(f, "Unexpected data found while parsing"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Error::FileNotOpenable(src) => Some(src),
            Error::HeaderNotParsable(src) => Some(src),
            Error::DataNotParsable(src) => Some(src),
            Error::FailedWrite(src) => Some(src),
            Error::UnexpectedData => None,
        }
    }
}
