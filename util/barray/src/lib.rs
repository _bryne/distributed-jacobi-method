//! Simple library to read and write multidimensional arrays of double
//! precision floating point numbers (IEEE 754) from and to binary files.
//!
//! A binary file has an header section starting with a 4 byte unsigned integer
//! ([u32]) describing the number of array dimensions. This number is followed by
//! one [u32] for every dimension defining its dimension size. The header section
//! ends with zeroed pad bytes to align the subsequent data section to the size
//! of one double precision floating point ([f64]) data element. The data section
//! is the concatenation of all [f64] elements of the array.
//!
//! ```
//! ---------------------------------------------------------------------
//! | dnum | dsize_0 | ... | dsize_n | (padding) | elem_0 | elem_1 | ...
//! ---------------------------------------------------------------------
//! ```
//!
//! # Warning
//!
//! All binary data is written in native byte order. Keep that in mind
//! when machines with different byte orders access the binary data!

use ndarray::{Array, ArrayD, Dimension, ShapeBuilder};
use std::fs::File;
use std::io;
use std::io::{BufReader, BufWriter, Read, Seek, Write};
use std::mem::size_of;

mod error;

/// Get the byte difference between the current address and the next address
/// that is aligned to the specified alignment requirement
fn get_pad_size(current: usize, align: usize) -> usize {
    if align.is_power_of_two() == false {
        panic!("alignment not a power of two");
    }

    (align - (current % align)) % align
}

/// Parse array header and return an array of all dimension sizes.
///
/// # Errors
///
/// Returns an error if the read fails.
///
/// # Panics
///
/// Panics if [usize] can not hold an [u32].
fn parse_array_header<R: Read + Seek>(reader: &mut BufReader<R>) -> Result<Vec<usize>, io::Error> {
    let mut buf = [0u8; 4];

    reader.read_exact(&mut buf)?;

    let ndim: usize = u32::from_ne_bytes(buf).try_into().unwrap();

    let mut dims = Vec::with_capacity(ndim);

    for _ in 0..ndim {
        reader.read_exact(&mut buf)?;
        dims.push(u32::from_ne_bytes(buf).try_into().unwrap());
    }

    Ok(dims)
}

/// Skip bytes until the next aligned address is reached. No bytes are skipped
/// if the current address is already aligned.
///
/// # Errors
///
/// Returns an error if the seek fails.
///
/// # Panics
///
/// Panics if [usize] is bigger than [i64].
fn skip_padding<R: Read + Seek>(reader: &mut BufReader<R>, align: usize) -> Result<(), io::Error> {
    let cur_pos: usize = reader.stream_position()?.try_into().unwrap();

    reader.seek_relative(get_pad_size(cur_pos, align).try_into().unwrap())?;

    Ok(())
}

/// Write zeroed bytes into the buffer until the next address that is aligned 
/// to the specified alignment is reached.
///
/// # Errors
///
/// Returns an error if the write fails or the current stream position cannot
/// be determined.
///
/// # Panics
///
/// Panics if [std::io::Seek::stream_position] panics or if [usize] can not
/// hold an [u64].
fn write_padding<W: Write + Seek>(
    writer: &mut BufWriter<W>,
    align: usize,
) -> Result<(), io::Error> {
    const PAD_BYTE: [u8; 1] = [0u8];
    while usize::try_from(writer.stream_position()?).unwrap() % align != 0 {
        writer.write_all(&PAD_BYTE)?;
    }

    Ok(())
}

/// Read the array data from the buffer.
///
/// # Errors
///
/// Returns an error if a read fails.
fn read_array_data<R: Read + Seek>(
    reader: &mut BufReader<R>,
    dims: Vec<usize>,
) -> Result<ArrayD<f64>, io::Error> {
    let mut array = ArrayD::zeros(dims.f());
    let mut buf = [0u8; 8];

    for elem in array.iter_mut() {
        reader.read_exact(&mut buf)?;
        *elem = f64::from_ne_bytes(buf);
    }

    Ok(array)
}

/// Write the array data to the buffer.
///
/// # Errors
///
/// Returns an error of a write fails.
fn write_array_data<D: Dimension, W: Write>(
    writer: &mut BufWriter<W>,
    array: &Array<f64, D>,
) -> Result<(), io::Error> {
    for elem in array.iter() {
        writer.write_all(&elem.to_ne_bytes())?;
    }

    Ok(())
}

/// Write the array header to the buffer.
///
/// # Errors
///
/// Returns an error of a write fails.
///
/// # Panics
///
/// Panics if each given array dimensions do not fit into an [u32].
fn write_array_header<D: Dimension, W: Write>(
    writer: &mut BufWriter<W>,
    array: &Array<f64, D>,
) -> Result<(), io::Error> {
    let mut buf = u32::try_from(array.ndim())
        .expect("Number of dimensions does not fit into u32")
        .to_ne_bytes();

    writer.write_all(&buf)?;

    for dim in array.shape().iter() {
        buf = u32::try_from(*dim)
            .expect("Size of dimension does not fit into u32")
            .to_ne_bytes();
        writer.write_all(&buf)?;
    }

    Ok(())
}

/// Read the array from the given file.
///
/// # Errors
///
/// Returns an error if the file cannot be opened, an parse error happens or if
/// there is more data than expected.
pub fn read_array(path: &str) -> Result<ArrayD<f64>, error::Error> {
    let file = match File::open(path) {
        Ok(f) => f,
        Err(err) => return Err(error::Error::FileNotOpenable(err)),
    };

    let mut reader = BufReader::new(file);

    let dims =
        parse_array_header(&mut reader).map_err(|src| error::Error::HeaderNotParsable(src))?;

    skip_padding(&mut reader, size_of::<f64>())
        .map_err(|src| error::Error::HeaderNotParsable(src))?;

    let array =
        read_array_data(&mut reader, dims).map_err(|src| error::Error::DataNotParsable(src))?;

    let mut buf = [0u8; 1];

    // Check for unexpected data after all expected data is read
    if let Ok(0) = reader.read(&mut buf) {
        return Ok(array);
    } else {
        return Err(error::Error::UnexpectedData);
    }
}

/// Write the array to the given file.
///
/// # Errors
///
/// Returns an error if the file cannot be opened or a write fails.
pub fn write_array<D: Dimension>(path: &str, array: &Array<f64, D>) -> Result<(), error::Error> {
    let file = match File::create(path) {
        Ok(f) => f,
        Err(err) => return Err(error::Error::FileNotOpenable(err)),
    };

    let mut writer = BufWriter::new(file);

    write_array_header(&mut writer, array).map_err(|src| error::Error::FailedWrite(src))?;
    write_padding(&mut writer, size_of::<f64>()).map_err(|src| error::Error::FailedWrite(src))?;
    write_array_data(&mut writer, array).map_err(|src| error::Error::FailedWrite(src))?;

    Ok(())
}


