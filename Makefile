SHELL = /bin/sh

EXEC ?= jac

OMPI_CC = clang
CC = mpicc

BLD = ./build
OBJ = $(BLD)/obj
SRC = ./src
INC = ./inc
DAT = ./data

SRCS := $(wildcard $(SRC)/*.c)
OBJS := $(patsubst $(SRC)/%.c,$(OBJ)/%.o,$(SRCS))
DATS := $(DAT)/A.bin $(DAT)/b.bin $(DAT)/x.bin

CFLAGS := -I$(INC) -Wall -Wextra -O3 -fno-tree-vectorize
LFLAGS := -lm

# Always generate new system of linear equations when SIZE is set. Otherwise
# generate a system of linear equations using the default size if there is
# none yet.
ifdef SIZE
	GEN = $(DAT)/%.bin
else
	# Default size
	SIZE = 1337
	GEN = $(DATS)
endif

$(BLD)/$(EXEC): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LFLAGS)

$(OBJ)/%.o: $(SRC)/%.c $(OBJ)
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ): $(BLD)
	mkdir -p $@

$(BLD):
	mkdir -p $@

$(DAT)/%.bin: 
	cd ./util/barray && cargo run --bin gen --release -- $(SIZE) ../../data

.PHONY: test clean cleandat

test: $(BLD)/$(EXEC) $(GEN)
	mpirun --mca opal_warn_on_missing_libcuda 0 build/jac -s data/A.bin data/b.bin
	cd ./util/barray && cargo run --bin cmp --release -- ../../result.bin ../../data/x.bin 1e-4

clean:
	$(RM) -r $(BLD)

cleandat:
	$(RM) -r $(DATS)


